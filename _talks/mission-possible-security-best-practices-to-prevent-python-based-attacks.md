---
duration: 40
presentation_url: null
room: PennTop South
slot: 2019-10-05 11:15:00-04:00
speakers:
- Katie Rosman
title: 'Mission Possible: Security Best Practices to Prevent Python-Based Attacks '
type: talk
video_url: null
---

As a popular open-source language, Python draws an international community
of active users and contributors. Yet Python’s global reach and scale also
make it a powerful target for pernicious, and potentially deadly, attacks.
Join us as we analyze some of the world's most well-known Python-based
historical attacks, from the security vulnerabilities that were exploited to
the damage that ensued. We will then break down today's most common types of
Python-based attacks, and describe specific remedies to guard against them.
This talk is intended for anyone who wants an easily digestible guide to
security weaknesses in Python and a concrete roadmap to optimizing the
security of their own code and applications.
